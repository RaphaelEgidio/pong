package br.com.pong;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;

public class game extends Application {
    private static final Shape[] shapes = {
            new Circle(50, Color.AQUAMARINE),
            new Rectangle(100, 100, Color.PALEGREEN)
    };

    private static final DropShadow highlight =
            new DropShadow(20, Color.GOLDENROD);

    @Override
    public void start(Stage stage) throws Exception {
        HBox layout = new HBox(40);
        layout.setPadding(new Insets(30));
        layout.setAlignment(Pos.CENTER);

        Label highlightedShapeLabel = new Label(" ");
        highlightedShapeLabel.setStyle(
                "-fx-font-family: monospace; -fx-font-size: 80px; -fx-text-fill: olive"
        );

        for (Shape shape: shapes) {
            layout.getChildren().add(shape);

            shape.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    shape.setEffect(highlight);
                    int idx = layout.getChildren().indexOf(shape) + 1;
                    highlightedShapeLabel.setText(
                            "" + idx
                    );
                }
            });

            shape.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    shape.setEffect(null);
                    highlightedShapeLabel.setText(" ");
                }
            });
        }

        layout.getChildren().add(highlightedShapeLabel);

        stage.setScene(new Scene(layout));
        stage.show();
    }

    public static void main(String[] args) { launch(args); }
}
