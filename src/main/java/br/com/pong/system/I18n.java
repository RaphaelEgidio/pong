package br.com.pong.system;

import javafx.scene.Node;
import javafx.scene.control.Tooltip;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public final class I18n {

    public static final String PROPERTIES_NAME = "i18n" + File.separator + "message";

    private I18n() {
        throw new AssertionError();
    }

    public static ResourceBundle getResourceBundle() throws IOException {
        return ResourceBundle.getBundle(I18n.PROPERTIES_NAME, I18n.getLocale());
    }

    public static String getText(final String properties, final Object... args) {
        return MessageFormat.format(I18n.getText(properties), args);
    }

    public static String getText(final String properties) {
        try {
            return getResourceBundle().getString(properties);
        } catch (Exception e) {
            return properties;
        }
    }

    public static Tooltip getTooltip(final String properties) {
        return new Tooltip(I18n.getText(properties));
    }

    public static Tooltip getTooltip(final String properties, final Object... args) {
        return new Tooltip(I18n.getText(properties, args));
    }

    public static void setTooltip(final Node node, final String properties, final Object... args) {
        Tooltip.install(node, I18n.getTooltip(properties, args));
    }

    public static void setTooltip(final Node node, final String properties) {
        Tooltip.install(node, I18n.getTooltip(properties));
    }

    public static List<Locale> getSupportedLocales() {
        return Arrays.asList(Locale.ENGLISH, new Locale("pt", "BR"));
    }

    public static Locale getLocale() {
        return Locale.getDefault();
    }

    public static void setLocale(final Locale locale) {
        Locale.setDefault(locale);
    }

}
