package br.com.pong.system;

import br.com.pong.PongApplication;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;

public final class Dialog {

    private Dialog() {
        throw new AssertionError();
    }

    private static Alert createAlert(final AlertType alertType, final AppImage appImage) {
        final Alert alert = new Alert(alertType);

        final Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        stage.toFront();

        PongApplication.VIEW.setIcon((Stage) alert.getDialogPane().getScene().getWindow(), appImage);

        return alert;
    }

    public static ButtonType alert(final AlertType alertType, final String title, final String content) {
        return Dialog.alert(alertType, title, null, content, null);
    }

    public static ButtonType alert(final AlertType alertType, final String title, final String header, final String content) {
        return Dialog.alert(alertType, title, header, content, null);
    }

    public static ButtonType alert(final AlertType alertType, final String title, final String header, final String content, final ButtonType... buttonTypes) {
        final Alert alert = createAlert(alertType, AppImage.APP_LOGO);

        alert.setTitle(I18n.getText(title));
        alert.setHeaderText(I18n.getText(header));
        alert.setContentText(I18n.getText(content));

        if (Objects.nonNull(buttonTypes) && buttonTypes.length > 0) {
            alert.getButtonTypes().setAll(buttonTypes);
        }

        return alert.showAndWait().get();
    }

    public static ButtonType exception(final String title, final String header, final String content, final Exception exception) {
        final Alert alert = createAlert(AlertType.ERROR, AppImage.APP_LOGO);

        alert.setTitle(I18n.getText(title));
        alert.setHeaderText(I18n.getText(header));
        alert.setContentText(I18n.getText(content));

        final StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));

        final TextArea textArea = new TextArea(sw.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        final GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(new Label(I18n.getText("")), 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        return alert.showAndWait().get();
    }

    public static String input(final String title, final String header, final String content) {
        final TextInputDialog textInputDialog = new TextInputDialog();

        textInputDialog.setTitle(I18n.getText(title));
        textInputDialog.setHeaderText(I18n.getText(header));
        textInputDialog.setContentText(I18n.getText(content));

        return textInputDialog.showAndWait().get();
    }

    public static String choise(final String title, final String header, final String content, final String valueDefault, final List<String> choises) {
        final ChoiceDialog<String> choiceDialog = new ChoiceDialog<>(valueDefault, choises);

        choiceDialog.setTitle(I18n.getText(title));
        choiceDialog.setHeaderText(I18n.getText(header));
        choiceDialog.setContentText(I18n.getText(content));

        return choiceDialog.showAndWait().get();
    }

}