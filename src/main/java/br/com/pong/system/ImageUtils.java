package br.com.pong.system;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.util.Objects;


public final class ImageUtils {

    private ImageUtils() {
        throw new AssertionError();
    }

    public static final String PATH_IMAGE = File.separator + "images" + File.separator;

    public static Image getImage(final AppImage appImage) {
        return new Image(ImageUtils.class.getClassLoader().getResourceAsStream(ImageUtils.PATH_IMAGE + appImage.getName()));
    }

    public static ImageView getImageView(final AppImage appImage, final boolean isFill) {
        final ImageView imageView = new ImageView(ImageUtils.getImage(appImage));

        if (isFill && Objects.nonNull(appImage.getWidth()) && Objects.nonNull(appImage.getHeigth())) {
            imageView.setFitWidth(appImage.getWidth());
            imageView.setFitHeight(appImage.getHeigth());
        }

        return imageView;
    }

    public static void setImageButton(final Button button, final AppImage image, final boolean setFill) {
        button.setGraphic(ImageUtils.getImageView(image, setFill));
    }

}
