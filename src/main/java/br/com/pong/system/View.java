package br.com.pong.system;

import br.com.pong.controller.AbstractController;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.Objects;

@Log4j2
public final class View {

    private Stage stage;
    private ConfigurableApplicationContext applicationContext;

    public View() {
    }

    public void loadView(final Screen screen) throws IOException {
        loadView(screen, null);
    }

    public void loadView(final Screen screen, final Object... data) throws IOException {
        final FXMLLoader fxmlLoader = getFXMLLoader(screen);
        this.stage.setScene(new Scene(fxmlLoader.load()));

        // Se tamanho da tela foi alterada mantem configuração para proxima tela
        if (this.stage.isMaximized()) {
            final Rectangle2D rectangle2D = javafx.stage.Screen.getPrimary().getVisualBounds();

            this.stage.setX(rectangle2D.getMinX());
            this.stage.setY(rectangle2D.getMinY());
            this.stage.setWidth(rectangle2D.getWidth());
            this.stage.setHeight(rectangle2D.getHeight());

        } else {

            this.stage.setMinWidth(screen.getWidth());
            this.stage.setMinHeight(screen.getHeight());

        }

        setDataController(fxmlLoader, data);

        this.stage.show();
    }

    public void loadViewDialog(final Screen screen) throws IOException {
        loadViewDialog(screen, null);
    }

    public void loadViewDialog(final Screen screen, final Object... data) throws IOException {
        final FXMLLoader fxmlLoader = getFXMLLoader(screen);

        final Stage dialog = new Stage();
        dialog.toFront();
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.setTitle(I18n.getText(screen.getTitle()));
        dialog.setAlwaysOnTop(true);
        dialog.setScene(new Scene((Parent) fxmlLoader.load(), screen.getWidth(), screen.getHeight()));
        dialog.initOwner(this.stage);

        setDataController(fxmlLoader, data);

        dialog.showAndWait();
    }

    public static void setIcon(final Stage stage, final AppImage appImage) {
        try {
            stage.getIcons().add(ImageUtils.getImage(appImage));

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }

    public void setApplicationContext(final ConfigurableApplicationContext applicationContext) {
        if (Objects.isNull(this.applicationContext)) {
            this.applicationContext = applicationContext;
        }
    }

    public void setStage(final Stage stage) {
        if (Objects.isNull(this.stage)) {
            this.stage = stage;
            setIcon(this.stage, AppImage.APP_LOGO);
            this.stage.setTitle(I18n.getText("application.title"));
        }
    }

    private void setDataController(final FXMLLoader fxmlLoader, final Object[] data) {
        if (Objects.nonNull(data) && data.length > 0) {
            AbstractController abstractController = fxmlLoader.getController();
            abstractController.setData(data);
        }
    }

    private FXMLLoader getFXMLLoader(final Screen screen) throws IOException {
        final FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(screen.getPath()), I18n.getResourceBundle());

        fxmlLoader.setControllerFactory(this.applicationContext::getBean);

        return fxmlLoader;
    }

}