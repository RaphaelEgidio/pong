package br.com.pong.system;

import lombok.Getter;

@Getter
public enum AppImage {

    APP_LOGO("pong.png", null, null);

    private String name;
    private Double width;
    private Double heigth;

    private AppImage(final String name, final Double width, final Double height) {
        this.name = name;
        this.width = width;
        this.heigth = height;
    }

}
