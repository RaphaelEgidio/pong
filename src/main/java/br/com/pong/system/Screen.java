package br.com.pong.system;

import lombok.Getter;

import java.util.Objects;

@Getter
public enum Screen {

    MAIN("/views/Main.fxml", 800d, 600d, ""),
    PONG("/views/Pong.fxml", 800d, 600d, "");

    private String path;
    private Double width;
    private Double height;
    private String title;

    private Screen(final String path, final Double width, final Double height, final String title) {
        this.path = path;
        this.width = width;
        this.height = height;
        this.title = title;
    }

    public Double getWidth() {
        return Objects.nonNull(this.width) ? this.width : 800;
    }

    public Double getHeight() {
        return Objects.nonNull(this.height) ? this.height : 600;
    }

}
