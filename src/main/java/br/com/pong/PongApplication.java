package br.com.pong;

import br.com.pong.system.Screen;
import br.com.pong.system.View;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PongApplication extends Application {

    public static final View VIEW = new View();

    @Override
    public void start(final Stage primaryStage) throws Exception {
        PongApplication.VIEW.setStage(primaryStage);
        PongApplication.VIEW.loadView(Screen.MAIN);
    }

    @Override
    public void init() {
        final String[] args = this.getParameters().getRaw().stream().toArray(String[]::new);
        PongApplication.VIEW.setApplicationContext(SpringApplication.run(PongApplication.class, args));
    }

    @Override
    public void stop() {
        System.exit(1);
    }

    public static void main(final String[] args) {
        launch(args);
    }

}
