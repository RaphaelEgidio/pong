package br.com.pong.controller;

import br.com.pong.PongApplication;
import br.com.pong.system.Dialog;
import br.com.pong.system.Screen;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Log4j2
@Controller
public class MainController implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void play() {
        try {
            PongApplication.VIEW.loadView(Screen.PONG);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
            Dialog.alert(Alert.AlertType.ERROR, "title.error", "msg.unable.load.screen");
        }
    }

    @FXML
    public void exit() {
        final ButtonType buttonType = Dialog.alert(Alert.AlertType.CONFIRMATION, "title.warning", "content.exit.game");
        if (buttonType.equals(ButtonType.OK)) {
            System.exit(1);
        }
    }

}
