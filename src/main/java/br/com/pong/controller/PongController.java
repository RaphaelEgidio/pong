package br.com.pong.controller;

import br.com.pong.PongApplication;
import br.com.pong.system.Dialog;
import br.com.pong.system.Screen;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Log4j2
@Controller
public class PongController implements Initializable {

    @FXML
    private Pane pane;
    @FXML
    private Label labelPoint1;
    @FXML
    private Label labelPoint2;

    private final Rectangle rectangle1 = new Rectangle(50, 150, Color.GREEN);
    private final Rectangle rectangle2 = new Rectangle(50, 150, Color.BLACK);

    private final Rectangle rectangle1Pinter = new Rectangle(10, 500, Color.GREEN);
    private final Rectangle rectangle2Pinter = new Rectangle(10, 500, Color.BLACK);

    private final Circle circle = new Circle(25.0f, Color.RED);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.labelPoint1.setText("0");
        this.labelPoint2.setText("0");

        this.circle.relocate(150, 150);

        this.rectangle1.setLayoutX(10);
        this.rectangle1.setLayoutY(10);
        this.rectangle2.setTranslateX(740);
        this.rectangle2.setTranslateY(10);

        this.rectangle1Pinter.setLayoutX(0);
        this.rectangle1Pinter.setLayoutY(10);
        this.rectangle2Pinter.setTranslateX(790);
        this.rectangle2Pinter.setTranslateY(10);

        this.pane.getChildren().addAll(circle, rectangle1, rectangle2, rectangle1Pinter, rectangle2Pinter);

        final Timeline timeline = new Timeline(new KeyFrame(Duration.millis(20),
                new EventHandler<ActionEvent>() {
                    double dx = 10;
                    double dy = 2;

                    @Override
                    public void handle(final ActionEvent actionEvent) {
                        circle.setLayoutX(circle.getLayoutX() + dx);
                        circle.setLayoutY(circle.getLayoutY() + dy);

                        final Bounds bounds = pane.getBoundsInLocal();

                        if (circle.getLayoutX() <= (bounds.getMinX() + circle.getRadius()) ||
                                circle.getLayoutX() >= (bounds.getMaxX() - circle.getRadius())) {
                            dx = -dx;
                        }
                        if ((circle.getLayoutY() >= (bounds.getMaxY() - circle.getRadius())) ||
                                (circle.getLayoutY() <= (bounds.getMinY() + circle.getRadius()))) {
                            dy = -dy;
                        }

                        if (Shape.intersect(circle, rectangle1).getBoundsInLocal().getWidth() != -1) {
                            dx = -dx;
                        }
                        if (Shape.intersect(circle, rectangle2).getBoundsInLocal().getWidth() != -1) {
                            dx = -dx;
                        }
                        scorePoint();
                    }
                }));

        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    @FXML
    public synchronized void move(final KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.W) && this.rectangle1.getTranslateY() >= 50) {
            this.rectangle1.setTranslateY(this.rectangle1.getTranslateY() - 50);
        }
        if (keyEvent.getCode().equals(KeyCode.S) && this.rectangle1.getTranslateY() <= 300) {
            this.rectangle1.setTranslateY(this.rectangle1.getTranslateY() + 50);
        }
        if (keyEvent.getCode().equals(KeyCode.UP) && this.rectangle2.getTranslateY() >= 50) {
            this.rectangle2.setTranslateY(this.rectangle2.getTranslateY() - 50);
        }
        if (keyEvent.getCode().equals(KeyCode.DOWN) && this.rectangle2.getTranslateY() <= 350) {
            this.rectangle2.setTranslateY(this.rectangle2.getTranslateY() + 50);
        }
    }

    @FXML
    public void backMenu() {
        try {
            PongApplication.VIEW.loadView(Screen.MAIN);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
            Dialog.alert(Alert.AlertType.ERROR, "title.error", "msg.unable.load.screen");
        }
    }

    private void sumPoint(final Label label) {
        label.setText(String.valueOf(Integer.valueOf(label.getText()) + 1));
    }

    private void scorePoint() {
        if (Shape.intersect(circle, rectangle1Pinter).getBoundsInLocal().getWidth() != -1) {
            sumPoint(this.labelPoint2);
        }
        if (Shape.intersect(circle, rectangle2Pinter).getBoundsInLocal().getWidth() != -1) {
            sumPoint(this.labelPoint1);
        }
    }

}
