package br.com.pong.controller;

public abstract class AbstractController {

    public abstract void setData(final Object... args);

}
